
#include <Windows.h>
#include <Psapi.h>

#include "RedoBlHooks.hpp"

bool init(){
	BlInit;
	
	ADDR BlScanHex(addrServerSetFov, "F3 0F 11 05 ? ? ? ? F3 0F 11 05 ? ? ? ? E9 ? ? ? ? 8B 76 34 8B CE FF 15 ? ? ? ? 8B CB FF D6 F3 0F 10 05 ? ? ? ? 84 C0 74 03 0F 57 C0 F3 0F 11 87 ? ? ? ?");
	
	unsigned int len = 16;
	for(unsigned int i = 0; i<len; i++){
		BYTE* loc = (BYTE*)(addrServerSetFov + i);
		BlPatchByte(loc,  0x90);
	}
	
	BlPrintf("NoFov: Successfully disabled serverSetFov.");
	
	return true;
}

int __stdcall DllMain( HINSTANCE hInstance,  unsigned long reason,  void *reserved ){
	switch(reason){
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return true;
		default:
			return true;
	}
}
